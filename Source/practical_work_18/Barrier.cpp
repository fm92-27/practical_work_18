// Fill out your copyright notice in the Description page of Project Settings.


#include "Barrier.h"
#include "SnakeBase.h"
#include "Components/SphereComponent.h"

// Sets default values
ABarrier::ABarrier()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USphereComponent* SphereBarrier = CreateDefaultSubobject<USphereComponent>(TEXT("Barrier"));
	RootComponent = SphereBarrier;

	bBerrier = false;
	bIsDestroy = true;

}

// Called when the game starts or when spawned
void ABarrier::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(10);
	
}

// Called every frame
void ABarrier::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();

}

void ABarrier::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
			bIsDestroy = false;
		}
	}
}

void ABarrier::Move()
{
	if (bIsDestroy)
	{
		bBerrier = !bBerrier;
		FVector ResultVector = ResultVectorFromRange();
		SetActorLocation(ResultVector);
		SetActorHiddenInGame(bBerrier);
	}
}
