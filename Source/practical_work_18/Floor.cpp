// Fill out your copyright notice in the Description page of Project Settings.


#include "Floor.h"

#include "Components/StaticMeshComponent.h"

// Sets default values
AFloor::AFloor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Floor = CreateDefaultSubobject<USceneComponent>(TEXT("Floor"));
	RootComponent = Floor;

	MeshFloor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshFloor"));
	MeshFloor->AttachTo(RootComponent);

	FVector NewLocation(400, 400, -50);
	FVector NewScale(4, 4, 0);
	MeshFloor->SetWorldLocation(NewLocation);
	MeshFloor->SetWorldScale3D(NewScale);

}

// Called when the game starts or when spawned
void AFloor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFloor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

