// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"

// Add default functionality here for any IInteractable functions that are not pure virtual.

void IInteractable::Interact(AActor* Interactor, bool bIsHead)
{
	
}

FVector IInteractable::ResultVectorFromRange()
{
	FVector ResultVector(0, 0, 0);

	ResultVector.X = (FMath::RandRange(-400, 400) / 100) * 100;
	ResultVector.Y = (FMath::RandRange(-400, 400) / 100) * 100;

	return ResultVector;
}
