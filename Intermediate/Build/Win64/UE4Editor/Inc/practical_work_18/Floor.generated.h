// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PRACTICAL_WORK_18_Floor_generated_h
#error "Floor.generated.h already included, missing '#pragma once' in Floor.h"
#endif
#define PRACTICAL_WORK_18_Floor_generated_h

#define practical_work_18_Source_practical_work_18_Floor_h_12_SPARSE_DATA
#define practical_work_18_Source_practical_work_18_Floor_h_12_RPC_WRAPPERS
#define practical_work_18_Source_practical_work_18_Floor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define practical_work_18_Source_practical_work_18_Floor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFloor(); \
	friend struct Z_Construct_UClass_AFloor_Statics; \
public: \
	DECLARE_CLASS(AFloor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/practical_work_18"), NO_API) \
	DECLARE_SERIALIZER(AFloor)


#define practical_work_18_Source_practical_work_18_Floor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAFloor(); \
	friend struct Z_Construct_UClass_AFloor_Statics; \
public: \
	DECLARE_CLASS(AFloor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/practical_work_18"), NO_API) \
	DECLARE_SERIALIZER(AFloor)


#define practical_work_18_Source_practical_work_18_Floor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFloor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFloor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloor(AFloor&&); \
	NO_API AFloor(const AFloor&); \
public:


#define practical_work_18_Source_practical_work_18_Floor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloor(AFloor&&); \
	NO_API AFloor(const AFloor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFloor)


#define practical_work_18_Source_practical_work_18_Floor_h_12_PRIVATE_PROPERTY_OFFSET
#define practical_work_18_Source_practical_work_18_Floor_h_9_PROLOG
#define practical_work_18_Source_practical_work_18_Floor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	practical_work_18_Source_practical_work_18_Floor_h_12_PRIVATE_PROPERTY_OFFSET \
	practical_work_18_Source_practical_work_18_Floor_h_12_SPARSE_DATA \
	practical_work_18_Source_practical_work_18_Floor_h_12_RPC_WRAPPERS \
	practical_work_18_Source_practical_work_18_Floor_h_12_INCLASS \
	practical_work_18_Source_practical_work_18_Floor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define practical_work_18_Source_practical_work_18_Floor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	practical_work_18_Source_practical_work_18_Floor_h_12_PRIVATE_PROPERTY_OFFSET \
	practical_work_18_Source_practical_work_18_Floor_h_12_SPARSE_DATA \
	practical_work_18_Source_practical_work_18_Floor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	practical_work_18_Source_practical_work_18_Floor_h_12_INCLASS_NO_PURE_DECLS \
	practical_work_18_Source_practical_work_18_Floor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PRACTICAL_WORK_18_API UClass* StaticClass<class AFloor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID practical_work_18_Source_practical_work_18_Floor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
