// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PRACTICAL_WORK_18_practical_work_18GameModeBase_generated_h
#error "practical_work_18GameModeBase.generated.h already included, missing '#pragma once' in practical_work_18GameModeBase.h"
#endif
#define PRACTICAL_WORK_18_practical_work_18GameModeBase_generated_h

#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_SPARSE_DATA
#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_RPC_WRAPPERS
#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesApractical_work_18GameModeBase(); \
	friend struct Z_Construct_UClass_Apractical_work_18GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Apractical_work_18GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/practical_work_18"), NO_API) \
	DECLARE_SERIALIZER(Apractical_work_18GameModeBase)


#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesApractical_work_18GameModeBase(); \
	friend struct Z_Construct_UClass_Apractical_work_18GameModeBase_Statics; \
public: \
	DECLARE_CLASS(Apractical_work_18GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/practical_work_18"), NO_API) \
	DECLARE_SERIALIZER(Apractical_work_18GameModeBase)


#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Apractical_work_18GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Apractical_work_18GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Apractical_work_18GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Apractical_work_18GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Apractical_work_18GameModeBase(Apractical_work_18GameModeBase&&); \
	NO_API Apractical_work_18GameModeBase(const Apractical_work_18GameModeBase&); \
public:


#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Apractical_work_18GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Apractical_work_18GameModeBase(Apractical_work_18GameModeBase&&); \
	NO_API Apractical_work_18GameModeBase(const Apractical_work_18GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Apractical_work_18GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Apractical_work_18GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Apractical_work_18GameModeBase)


#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_12_PROLOG
#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_SPARSE_DATA \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_RPC_WRAPPERS \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_INCLASS \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_SPARSE_DATA \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PRACTICAL_WORK_18_API UClass* StaticClass<class Apractical_work_18GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID practical_work_18_Source_practical_work_18_practical_work_18GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
