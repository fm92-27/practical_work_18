// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "practical_work_18/Barrier.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBarrier() {}
// Cross Module References
	PRACTICAL_WORK_18_API UClass* Z_Construct_UClass_ABarrier_NoRegister();
	PRACTICAL_WORK_18_API UClass* Z_Construct_UClass_ABarrier();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_practical_work_18();
	PRACTICAL_WORK_18_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ABarrier::StaticRegisterNativesABarrier()
	{
	}
	UClass* Z_Construct_UClass_ABarrier_NoRegister()
	{
		return ABarrier::StaticClass();
	}
	struct Z_Construct_UClass_ABarrier_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsDestroy_MetaData[];
#endif
		static void NewProp_bIsDestroy_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsDestroy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBerrier_MetaData[];
#endif
		static void NewProp_bBerrier_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBerrier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABarrier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_practical_work_18,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABarrier_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Barrier.h" },
		{ "ModuleRelativePath", "Barrier.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABarrier_Statics::NewProp_bIsDestroy_MetaData[] = {
		{ "ModuleRelativePath", "Barrier.h" },
	};
#endif
	void Z_Construct_UClass_ABarrier_Statics::NewProp_bIsDestroy_SetBit(void* Obj)
	{
		((ABarrier*)Obj)->bIsDestroy = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABarrier_Statics::NewProp_bIsDestroy = { "bIsDestroy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ABarrier), &Z_Construct_UClass_ABarrier_Statics::NewProp_bIsDestroy_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABarrier_Statics::NewProp_bIsDestroy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABarrier_Statics::NewProp_bIsDestroy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABarrier_Statics::NewProp_bBerrier_MetaData[] = {
		{ "ModuleRelativePath", "Barrier.h" },
	};
#endif
	void Z_Construct_UClass_ABarrier_Statics::NewProp_bBerrier_SetBit(void* Obj)
	{
		((ABarrier*)Obj)->bBerrier = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABarrier_Statics::NewProp_bBerrier = { "bBerrier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ABarrier), &Z_Construct_UClass_ABarrier_Statics::NewProp_bBerrier_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABarrier_Statics::NewProp_bBerrier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABarrier_Statics::NewProp_bBerrier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABarrier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABarrier_Statics::NewProp_bIsDestroy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABarrier_Statics::NewProp_bBerrier,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABarrier_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABarrier, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABarrier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABarrier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABarrier_Statics::ClassParams = {
		&ABarrier::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ABarrier_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ABarrier_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABarrier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABarrier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABarrier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABarrier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABarrier, 4061326179);
	template<> PRACTICAL_WORK_18_API UClass* StaticClass<ABarrier>()
	{
		return ABarrier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABarrier(Z_Construct_UClass_ABarrier, &ABarrier::StaticClass, TEXT("/Script/practical_work_18"), TEXT("ABarrier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABarrier);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
