// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PRACTICAL_WORK_18_Barrier_generated_h
#error "Barrier.generated.h already included, missing '#pragma once' in Barrier.h"
#endif
#define PRACTICAL_WORK_18_Barrier_generated_h

#define practical_work_18_Source_practical_work_18_Barrier_h_13_SPARSE_DATA
#define practical_work_18_Source_practical_work_18_Barrier_h_13_RPC_WRAPPERS
#define practical_work_18_Source_practical_work_18_Barrier_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define practical_work_18_Source_practical_work_18_Barrier_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABarrier(); \
	friend struct Z_Construct_UClass_ABarrier_Statics; \
public: \
	DECLARE_CLASS(ABarrier, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/practical_work_18"), NO_API) \
	DECLARE_SERIALIZER(ABarrier) \
	virtual UObject* _getUObject() const override { return const_cast<ABarrier*>(this); }


#define practical_work_18_Source_practical_work_18_Barrier_h_13_INCLASS \
private: \
	static void StaticRegisterNativesABarrier(); \
	friend struct Z_Construct_UClass_ABarrier_Statics; \
public: \
	DECLARE_CLASS(ABarrier, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/practical_work_18"), NO_API) \
	DECLARE_SERIALIZER(ABarrier) \
	virtual UObject* _getUObject() const override { return const_cast<ABarrier*>(this); }


#define practical_work_18_Source_practical_work_18_Barrier_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABarrier(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABarrier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABarrier); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABarrier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABarrier(ABarrier&&); \
	NO_API ABarrier(const ABarrier&); \
public:


#define practical_work_18_Source_practical_work_18_Barrier_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABarrier(ABarrier&&); \
	NO_API ABarrier(const ABarrier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABarrier); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABarrier); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABarrier)


#define practical_work_18_Source_practical_work_18_Barrier_h_13_PRIVATE_PROPERTY_OFFSET
#define practical_work_18_Source_practical_work_18_Barrier_h_10_PROLOG
#define practical_work_18_Source_practical_work_18_Barrier_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	practical_work_18_Source_practical_work_18_Barrier_h_13_PRIVATE_PROPERTY_OFFSET \
	practical_work_18_Source_practical_work_18_Barrier_h_13_SPARSE_DATA \
	practical_work_18_Source_practical_work_18_Barrier_h_13_RPC_WRAPPERS \
	practical_work_18_Source_practical_work_18_Barrier_h_13_INCLASS \
	practical_work_18_Source_practical_work_18_Barrier_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define practical_work_18_Source_practical_work_18_Barrier_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	practical_work_18_Source_practical_work_18_Barrier_h_13_PRIVATE_PROPERTY_OFFSET \
	practical_work_18_Source_practical_work_18_Barrier_h_13_SPARSE_DATA \
	practical_work_18_Source_practical_work_18_Barrier_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	practical_work_18_Source_practical_work_18_Barrier_h_13_INCLASS_NO_PURE_DECLS \
	practical_work_18_Source_practical_work_18_Barrier_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PRACTICAL_WORK_18_API UClass* StaticClass<class ABarrier>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID practical_work_18_Source_practical_work_18_Barrier_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
